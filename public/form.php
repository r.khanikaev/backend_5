<!DOCTYPE html>
<html lang="ru">
    
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Backend 5</title>
    <link rel="stylesheet" href="style.css">
    <script src="script.js"></script>
  </head>
<body>
    <?php
    if (!empty($messages)) {
        print('<div id="messages">');
        // Выводим все сообщения.
        foreach ($messages as $message) {
            print($message);
        }
        print('</div>');
    }
    // Далее выводим форму отмечая элементы с ошибками классом error
    // и задавая начальные значения элементов ранее сохраненными.
    ?>
    <div class="form">
            <form action="" method="POST">
                <label><input name="fio" type="text"  placeholder="Name" <?php if ($errors['fio']) {print 'class="error"';} ?> value="<?php print $values['fio']; ?>" /></label>
                   <label><input name="email" type="email" placeholder="E-mail" <?php if ($errors['email']) {print 'class="error"';} ?> value="<?php print $values['email']; ?>" /></label>
                   <label><input type="date" id="date" name="birthday" <?php if ($errors['birthday']) {print 'class="error"';} ?> value="<?php print $values['birthday']; ?>" /></label>
                
                <?php if ($errors['gender']) {print '<div class="error">';} ?> 
                <p>Пол:</p>
                <div class="form_radio" >
                    <label><input type="radio" name="gender" value="Male" <?php if($values['gender']=='Male') print "checked";else print ""; ?> />
                       М
                    </label>
                </div>
                
                <div class="form_radio">
                    <label><input type="radio" name="gender" value="Female" <?php echo ($values['gender']=='Female') ?  "checked" : "" ;  ?> />
                        Ж
                    </label>
                </div>
                <?php if ($errors['gender']) {print '</div>';print $messages[3];} ?>
                
                <?php if ($errors['limbs']) {print '<div class="error">';} ?> 
                <p>Количество конечностей:</p>
                <div class="form_radio">
                    <label><input type="radio" name="limbs" value="one" <?php echo ($values['limbs']=='one') ?  "checked" : "" ;  ?> />
                        1
                    </label>
                </div>
                
                <div class="form_radio">
                    <label><input type="radio" name="limbs" value="two" <?php echo ($values['limbs']=='two') ?  "checked" : "" ;  ?> />
                        2
                    </label>
                </div>
                
                <div class="form-group2">
                <p> <select name="ability[]" multiple="multiple" <?php if ($errors['ability']) {print 'class="error"';} ?>>
                    <option value="1" <?php echo (in_array("1",$ability)) ?  "selected" : ""  ; ?>>бессмертие</option>
                    <option value="2" <?php echo (in_array('2',$ability)) ?  "selected" : ""  ; ?>>Прохождение сквозь стены</option>
                    <option value="3" <?php echo (in_array('3',$ability)) ?  "selected" : ""  ; ?>>Левитация </option>
                    <option value="4" <?php echo (in_array('4',$ability)) ?  "selected" : ""  ; ?>>Управление временем</option>
                    </select>
                </div>
                <?php if ($errors['ability']) {print $messages[5];} ?>
                    
                    <textarea name="biography" cols="26" rows="2" placeholder= "Биография" ></textarea>
                    <input type="checkbox" name="contract" value="allow"> <p>C контрактом ознакомлен</p>
                    <input class="submit1" type='submit' value='Submit'>
            </form>
    </div>

    <a href="login.php" style="color:white;  text-decoration: none">logout/login</a>
    
    <?php
    if (!empty($messages)) {
        print $messages[8];
        print $messages[9];
        if(!empty($_SESSION['login']))
            printf($messages[10], $_SESSION['login'], $_SESSION['uid']);
    }
    ?>


</body>
</html>
